# use monkeypatching for every test in test_auth.py where database class are made
import json

import pytest  # type: ignore
from flask import current_app

import src.api.auth
from src.api.users.models import User

# from datetime import datetime


def test_user_registration(test_app, monkeypatch):
    def mock_get_user_by_email(email):
        return None

    def mock_get_user_by_username(username):
        return None

    def mock_add_user(username, email, password):
        return User(username=username, email=email, password=password)

    monkeypatch.setattr(src.api.auth, "get_user_by_email", mock_get_user_by_email)
    monkeypatch.setattr(src.api.auth, "add_user", mock_add_user)
    monkeypatch.setattr(src.api.auth, "get_user_by_username", mock_get_user_by_username)

    client = test_app.test_client()
    resp = client.post(
        "/auth/register",
        data=json.dumps(
            {
                "username": "justatest",
                "email": "test@test.com",
                "password": "123456",
            }
        ),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 201
    assert resp.content_type == "application/json"
    assert "justatest" in data["username"]
    assert "test@test.com" in data["email"]
    assert "password" not in data


def test_user_registration_duplicate_email(test_app, add_user, monkeypatch):
    def mock_get_user_by_email(email):
        return True

    monkeypatch.setattr(src.api.auth, "get_user_by_email", mock_get_user_by_email)

    client = test_app.test_client()
    resp = client.post(
        "/auth/register",
        data=json.dumps(
            {"username": "michael", "email": "test@test.com", "password": "test"}
        ),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert resp.content_type == "application/json"
    assert "Sorry. That email already exists." in data["message"]


@pytest.mark.parametrize(
    "payload",
    [
        {},
        {"email": "me@testdriven.io", "password": "greaterthanten"},
        {"username": "michael", "password": "greaterthanten"},
        {"email": "me@testdriven.io", "username": "michael"},
    ],
)
def test_user_registration_invalid_json(test_app, payload):
    client = test_app.test_client()
    resp = client.post(
        "/auth/register",
        data=json.dumps(payload),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert resp.content_type == "application/json"
    assert "Input payload validation failed" in data["message"]


def test_registered_user_login(test_app, monkeypatch):
    class AttrDict(dict):
        def __init__(self, *args, **kwargs):
            super(AttrDict, self).__init__(*args, **kwargs)
            self.__dict__ = self

    def mock_get_user_by_email(email):
        d = AttrDict()
        d.update(
            {
                "id": 1,
                "username": "test3",
                "email": "test3@test.com",
                "password": "goodpassword",
            }
        )
        return d

    def mock_check_password_hash(user_pw, pw):
        return True

    monkeypatch.setattr(src.api.auth, "get_user_by_email", mock_get_user_by_email)
    monkeypatch.setattr(
        src.api.auth.bcrypt, "check_password_hash", mock_check_password_hash
    )

    client = test_app.test_client()
    resp = client.post(
        "/auth/login",
        data=json.dumps({"email": "test3@test.com", "password": "test"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert resp.content_type == "application/json"
    assert data["access_token"]
    assert data["refresh_token"]


def test_not_registered_user_login(test_app, monkeypatch):
    def mock_get_user_by_email(email):
        return False

    monkeypatch.setattr(src.api.auth, "get_user_by_email", mock_get_user_by_email)

    client = test_app.test_client()
    resp = client.post(
        "/auth/login",
        data=json.dumps({"email": "testnotreal@test.com", "password": "test"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert resp.content_type == "application/json"
    assert "User does not exist." in data["message"]


def test_valid_refresh(test_app, monkeypatch):
    class AttrDict(dict):
        def __init__(self, *args, **kwargs):
            super(AttrDict, self).__init__(*args, **kwargs)
            self.__dict__ = self

    def mock_get_user_by_id(user_id):
        d = AttrDict()
        d.update(
            {
                "id": 1,
                "username": "test4",
                "email": "test4@test.com",
                "password": "test",
            }
        )
        return d

    def mock_decode_token(user_id):
        return 1

    monkeypatch.setattr(src.api.auth.User, "decode_token", mock_decode_token)
    monkeypatch.setattr(src.api.auth, "get_user_by_id", mock_get_user_by_id)

    client = test_app.test_client()
    # valid refresh
    resp = client.post(
        "/auth/refresh",
        data=json.dumps({"refresh_token": "token"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert resp.content_type == "application/json"
    assert data["access_token"]
    assert data["refresh_token"]


def test_invalid_refresh_expired_token(test_app):

    current_app.config["REFRESH_TOKEN_EXPIRATION"] = -1
    client = test_app.test_client()

    # invalid token refresh
    refresh_token = User.encode_token(1, "refresh")
    resp = client.post(
        "/auth/refresh",
        data=json.dumps({"refresh_token": refresh_token}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 401
    assert resp.content_type == "application/json"
    assert "Signature expired. Please log in again." in data["message"]


def test_invalid_refresh(test_app):
    client = test_app.test_client()
    resp = client.post(
        "/auth/refresh",
        data=json.dumps({"refresh_token": "Invalid"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 401
    assert resp.content_type == "application/json"
    assert "Invalid token. Please log in again." in data["message"]


def test_refresh_invalid_user_in_token(test_app, monkeypatch):
    def mock_get_user_by_id(user_id):
        return None

    monkeypatch.setattr(src.api.auth, "get_user_by_id", mock_get_user_by_id)

    current_app.config["REFRESH_TOKEN_EXPIRATION"] = 3
    client = test_app.test_client()
    token = User.encode_token("999", "refresh")
    resp = client.post(
        "/auth/refresh",
        data=json.dumps({"refresh_token": token}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 401
    assert resp.content_type == "application/json"
    assert "Invalid token" in data["message"]


def test_user_status(test_app, monkeypatch):
    class AttrDict(dict):
        def __init__(self, *args, **kwargs):
            super(AttrDict, self).__init__(*args, **kwargs)
            self.__dict__ = self

    def mock_get_user_by_id(user_id):
        d = AttrDict()
        d.update(
            {
                "id": 1,
                "username": "test6",
                "email": "test6@test.com",
                "password": "test",
            }
        )
        return d

    monkeypatch.setattr(src.api.auth, "get_user_by_id", mock_get_user_by_id)

    client = test_app.test_client()
    token = User.encode_token(1, "access")
    resp = client.get(
        "/auth/status",
        headers={"Authorization": f"Bearer {token}"},
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert resp.content_type == "application/json"
    assert "test6" in data["username"]
    assert "test6@test.com" in data["email"]
    assert "password" not in data


def test_invalid_status(test_app):
    client = test_app.test_client()
    resp = client.get(
        "/auth/status",
        headers={"Authorization": "Bearer invalid"},
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 401
    assert resp.content_type == "application/json"
    assert "Invalid token. Please log in again." in data["message"]


def test_status_expired_token(test_app):

    current_app.config["ACCESS_TOKEN_EXPIRATION"] = -1
    client = test_app.test_client()
    token = User.encode_token(1, "access")
    resp = client.get(
        "/auth/status",
        headers={"Authorization": f"Bearer {token}"},
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 401
    assert resp.content_type == "application/json"
    assert "Signature expired. Please log in again." in data["message"]


def test_status_invalid_user_in_token(test_app, monkeypatch):
    def mock_get_user_by_id(user_id):
        return None

    monkeypatch.setattr(src.api.auth, "get_user_by_id", mock_get_user_by_id)

    current_app.config["ACCESS_TOKEN_EXPIRATION"] = 3
    client = test_app.test_client()
    token = User.encode_token("999", "access")
    resp = client.get(
        "/auth/status",
        headers={"Authorization": f"Bearer {token}"},
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 401
    assert resp.content_type == "application/json"
    assert "Invalid token" in data["message"]


def test_status_missing_token_in_header(test_app):
    client = test_app.test_client()
    resp = client.get(
        "/auth/status",
        headers={},
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 403
    assert resp.content_type == "application/json"
    assert "Token required" in data["message"]
